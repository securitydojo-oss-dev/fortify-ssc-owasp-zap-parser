# Fortify SSC Parser Plugin for ZAP

## Introduction

This Fortify SSC parser plugin allows for importing ZAP export results.

### Related Links

* **Downloads**: https://gitlab.com/securitydojo-oss-dev/fortify-ssc-zap-parser/-/releases
* **Sample input files**: [sampleData](sampleData)
* **GitLab**: https://gitlab.com/securitydojo-oss-dev/fortify-ssc-zap-parser


## Plugin Installation

These sections describe how to install, upgrade and uninstall the plugin.

### Install & Upgrade

* Obtain the plugin binary jar file
* If you already have another version of the plugin installed, first uninstall the previously
  installed version of the plugin by following the steps under [Uninstall](#uninstall) below
* In Fortify Software Security Center:
    * Navigate to Administration->Plugins->Parsers
    * Click the `NEW` button
    * Accept the warning
    * Upload the plugin jar file
    * Enable the plugin by clicking the `ENABLE` button

### Uninstall

* In Fortify Software Security Center:
    * Navigate to Administration->Plugins->Parsers
    * Select the parser plugin that you want to uninstall
    * Click the `DISABLE` button
    * Click the `REMOVE` button


## Results format

Note that the SSC parser plugin requires the uploaded reports to be in XML format.

## Upload results

As a 3<sup>rd</sup>-party results zip bundle:

* Generate a scan.info file containing a single line as follows:  
  `engineType=SECDOJO_ZAP`
* Generate a zip file containing the following:
    * The scan.info file generated in the previous step
    * The XML file containing scan results
* Upload the zip file generated in the previous step to SSC
    * Using any SSC client, for example FortifyClient or Maven plugin
    * Or using the SSC web interface
    * Similar to how you would upload an FPR file

## License

See [LICENSE.TXT](LICENSE.TXT)
