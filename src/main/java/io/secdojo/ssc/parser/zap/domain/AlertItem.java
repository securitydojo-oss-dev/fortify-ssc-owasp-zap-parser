package io.secdojo.ssc.parser.zap.domain;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;

@Getter
public class AlertItem {

    @JacksonXmlProperty(localName = "pluginid")
    String pluginId;
    @JacksonXmlProperty(localName = "alert")
    String alert;
    @JacksonXmlProperty(localName = "name")
    String name;
    @JacksonXmlProperty(localName = "riskcode")
    String riskCode;
    @JacksonXmlProperty(localName = "confidence")
    String confidence;
    @JacksonXmlProperty(localName = "riskdesc")
    String riskdesc;
    @JacksonXmlProperty(localName = "desc")
    String desc;
    @JacksonXmlElementWrapper(localName = "instances")
    @JacksonXmlProperty(localName = "instance")
    Instance[] instances;
    @JacksonXmlProperty(localName = "count")
    String count;
    @JacksonXmlProperty(localName = "solution")
    String solution;
    @JacksonXmlProperty(localName = "reference")
    String reference;
    @JacksonXmlProperty(localName = "cweid")
    String cweId;
    @JacksonXmlProperty(localName = "wascid")
    String wascId;
    @JacksonXmlProperty(localName = "sourceid")
    String sourceId;
    @JacksonXmlProperty(localName = "otherinfo")
    String otherInfo;
}
