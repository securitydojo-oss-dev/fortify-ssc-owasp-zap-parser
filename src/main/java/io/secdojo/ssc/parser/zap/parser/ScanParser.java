package io.secdojo.ssc.parser.zap.parser;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.secdojo.ssc.parser.zap.domain.Alerts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fortify.plugin.api.ScanBuilder;
import com.fortify.plugin.api.ScanData;
import com.fortify.plugin.api.ScanParsingException;
import com.fortify.util.ssc.parser.xml.ScanDataStreamingXmlParser;

public class ScanParser {
	private static final Logger LOG = LoggerFactory.getLogger(ScanParser.class);
	private final ScanData scanData;
	private final ScanBuilder scanBuilder;
    
	public ScanParser(final ScanData scanData, final ScanBuilder scanBuilder) {
		this.scanData = scanData;
		this.scanBuilder = scanBuilder;
	}
	
	public final void parse() throws ScanParsingException, IOException {

		new ScanDataStreamingXmlParser()
			.handler("/OWASPZAPReport/site/alerts", Alerts.class, this::processScanReport)
			.parse(scanData);

	}
	
	private void processScanReport(Alerts alerts) {
		LOG.info("Scan parser processScanReport starting");

		scanBuilder.setScanDate(new Date());
		scanBuilder.setEngineVersion("Zap");
		scanBuilder.completeScan();
	}

	
	private Date parseScanDate(String dateString) {
		Date date;
		try {
			LOG.warn("Scan Date: " + dateString);
			date = new SimpleDateFormat("dd MMM yyyy h:mma 'GMT'Z", Locale.ENGLISH).parse(dateString);
		} catch (ParseException e) {
			LOG.warn("Error parsing date " + dateString, e);
			date = new Date();
		}
		return date;
	}
}
