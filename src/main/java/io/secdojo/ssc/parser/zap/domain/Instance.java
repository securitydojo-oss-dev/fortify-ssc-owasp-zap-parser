package io.secdojo.ssc.parser.zap.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;

@Getter
public class Instance {

    @JacksonXmlProperty(localName = "uri")
    String uri;
    @JacksonXmlProperty(localName = "method")
    String method;
    @JacksonXmlProperty(localName = "param")
    String param;
    @JacksonXmlProperty(localName = "evidence")
    String evidence;

    public String getString() {
        StringBuilder sb = new StringBuilder();
        StringBuilder uriLink = new StringBuilder();
        uriLink.append("<a href=\"").append(uri).append("\">").append(uri).append("</a>");
        boldTitle(sb,"Uri", uriLink.toString());
        boldTitle(sb,"Method",this.method);
        boldTitle(sb,"Param",this.param);
        boldTitle(sb,"Evidence",this.evidence);
        return sb.toString();
    }
    private void boldTitle(StringBuilder sb, String title, String data){
        if(data != null) {
            if (!data.isEmpty()) {
                sb.append("<b>").append(title).append(" : </b>");
                sb.append(data);
                sb.append("</br>");
            }
        }
    }
}
