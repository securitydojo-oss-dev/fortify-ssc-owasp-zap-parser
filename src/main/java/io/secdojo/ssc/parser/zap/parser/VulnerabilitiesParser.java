package io.secdojo.ssc.parser.zap.parser;

import java.io.IOException;

import com.fortify.plugin.api.BasicVulnerabilityBuilder.Priority;
import com.fortify.plugin.api.ScanData;
import com.fortify.plugin.api.ScanParsingException;
import com.fortify.plugin.api.StaticVulnerabilityBuilder;
import com.fortify.plugin.api.VulnerabilityHandler;
import io.secdojo.ssc.parser.zap.CustomVulnAttribute;
import com.fortify.util.ssc.parser.EngineTypeHelper;
import com.fortify.util.ssc.parser.xml.ScanDataStreamingXmlParser;

import io.secdojo.ssc.parser.zap.domain.AlertItem;
import io.secdojo.ssc.parser.zap.domain.Alerts;
import io.secdojo.ssc.parser.zap.domain.Instance;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VulnerabilitiesParser {
	private static final Logger LOG = LoggerFactory.getLogger(VulnerabilitiesParser.class);
	private static final String ENGINE_TYPE = EngineTypeHelper.getEngineType();

	private final ScanData scanData;
	private final VulnerabilityHandler vulnerabilityHandler;

    public VulnerabilitiesParser(final ScanData scanData, final VulnerabilityHandler vulnerabilityHandler) {
    	this.scanData = scanData;
		this.vulnerabilityHandler = vulnerabilityHandler;
	}
    
    /**
	 * Main method to commence parsing the input provided by the configured {@link ScanData}.
	 * @throws ScanParsingException
	 * @throws IOException
	 */

	public final void parse() throws ScanParsingException, IOException {
		new ScanDataStreamingXmlParser()
			.handler("/OWASPZAPReport/site/alerts/alertitem", AlertItem.class, this::buildVulnerabilities)
			.parse(scanData);
	}

	public final void getVulnerabilities(Alerts alerts){
		AlertItem[] vulnerabilities = alerts.getAlertItems();
		LOG.info("Nb vulnerabilities : " + vulnerabilities.length);
		for (AlertItem vulnerability: vulnerabilities ) {
			this.buildVulnerabilities(vulnerability);
		}
	}
	
	
	private void buildVulnerabilities(AlertItem vulnerability) {
		StaticVulnerabilityBuilder vb = vulnerabilityHandler.startStaticVulnerability(this.getInstanceId(vulnerability));

		vb.setEngineType(ENGINE_TYPE);
		vb.setAnalyzer("ZAP");

		// Set mandatory values to JavaDoc-recommended values
		vb.setAccuracy(5.0f);
		vb.setLikelihood(Float.parseFloat(vulnerability.getConfidence()));

		vb.setCategory(vulnerability.getName());
		vb.setPriority(this.getPriority(vulnerability));

		vb.setStringCustomAttributeValue(CustomVulnAttribute.desc,vulnerability.getDesc());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.solution,vulnerability.getSolution());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.reference, this.linkReferences(vulnerability.getReference()));
		vb.setStringCustomAttributeValue(CustomVulnAttribute.cweId, vulnerability.getCweId());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.wascId, vulnerability.getWascId());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.riskdesc, vulnerability.getRiskdesc());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.otherInfo, vulnerability.getOtherInfo());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.instance, this.getInstance(vulnerability.getInstances()));


		vb.completeVulnerability();
    }

    private String getInstance(Instance[] instances){
		StringBuilder sb = new StringBuilder();
		for (Instance instance : instances ) {
			sb.append("<b>Instance :</b></br>").append(instance.getString()).append("</br></br>");
		}
		return sb.toString();
	}

	private  String getInstanceId(AlertItem vuln) {
		return DigestUtils.sha256Hex(vuln.getAlert() + vuln.getPluginId());
	}

	private Priority getPriority(AlertItem vuln) {
		float score = Float.parseFloat(vuln.getRiskCode());
		if(score < 3 ){
			return Priority.Low;
		}else if(score >= 3 && score < 6){
			return  Priority.Medium;
		}else if(score >= 6 && score < 9){
			return  Priority.High;
		}else {
			return  Priority.Critical;
		}
	}

	private String linkReferences(String reference){
		String[] references = StringUtils.substringsBetween(reference,"<p>", "</p>");
		StringBuilder sb = new StringBuilder();
		for (String ref : references ) {
			sb.append("<a href=\"").append(ref).append("\">").append(ref).append("</a>").append("</br>");
		}
		return sb.toString();
	}
}
