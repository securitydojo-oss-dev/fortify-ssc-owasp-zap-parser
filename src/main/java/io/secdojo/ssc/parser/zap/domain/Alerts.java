package io.secdojo.ssc.parser.zap.domain;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;

@Getter
public class Alerts {
    @JacksonXmlProperty(localName = "alertiem")
    AlertItem[] alertItems;
}
