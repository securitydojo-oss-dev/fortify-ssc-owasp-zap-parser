package io.secdojo.ssc.parser.zap;

public enum CustomVulnAttribute implements com.fortify.plugin.spi.VulnerabilityAttribute {
    
    pluginId(AttrType.STRING),
    alert(AttrType.STRING),
    name(AttrType.STRING),
    riskcode(AttrType.STRING),
    confidence(AttrType.STRING),
    riskdesc(AttrType.STRING),
    desc(AttrType.LONG_STRING),
    instance(AttrType.LONG_STRING),
    solution(AttrType.LONG_STRING),
    reference(AttrType.LONG_STRING),
    otherInfo(AttrType.LONG_STRING),
    cweId(AttrType.STRING),
    wascId(AttrType.STRING),
    sourceId(AttrType.STRING),
	;

    private final AttrType attributeType;

    CustomVulnAttribute(final AttrType attributeType) {
        this.attributeType = attributeType;
    }

    @Override
    public String attributeName() {
        return name();
    }

    @Override
    public AttrType attributeType() {
        return attributeType;
    }
}
