package io.secdojo.ssc.parser.zap;

import java.io.IOException;

import io.secdojo.ssc.parser.zap.parser.ScanParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fortify.plugin.api.ScanBuilder;
import com.fortify.plugin.api.ScanData;
import com.fortify.plugin.api.ScanParsingException;
import com.fortify.plugin.api.VulnerabilityHandler;
import com.fortify.plugin.spi.ParserPlugin;
import io.secdojo.ssc.parser.zap.parser.VulnerabilitiesParser;

public class ZAPParserPlugin implements ParserPlugin<CustomVulnAttribute> {
    private static final Logger LOG = LoggerFactory.getLogger(ZAPParserPlugin.class);

    @Override
    public void start() throws Exception {
        LOG.info("{} is starting", this.getClass().getSimpleName());
    }

    @Override
    public void stop() throws Exception {
        LOG.info("{} is stopping", this.getClass().getSimpleName());
    }

    @Override
    public Class<CustomVulnAttribute> getVulnerabilityAttributesClass() {
        return CustomVulnAttribute.class;
    }

    @Override
    public void parseScan(final ScanData scanData, final ScanBuilder scanBuilder) throws ScanParsingException, IOException {
        ScanParser scanParser = new ScanParser(scanData, scanBuilder);
        scanParser.parse();
    }

	@Override
	public void parseVulnerabilities(final ScanData scanData, final VulnerabilityHandler vulnerabilityHandler) throws ScanParsingException, IOException {
        VulnerabilitiesParser vulnerabilitiesParser = new VulnerabilitiesParser(scanData, vulnerabilityHandler);
        vulnerabilitiesParser.parse();
	}
}
