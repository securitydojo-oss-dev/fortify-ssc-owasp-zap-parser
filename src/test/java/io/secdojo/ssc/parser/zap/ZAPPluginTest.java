package io.secdojo.ssc.parser.zap;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

import org.junit.jupiter.api.Test;

import com.fortify.plugin.api.ScanBuilder;
import com.fortify.plugin.api.ScanData;
import com.fortify.plugin.api.ScanEntry;
import com.fortify.plugin.api.StaticVulnerabilityBuilder;
import com.fortify.plugin.api.VulnerabilityHandler;

class ZAPPluginTest {
	private static final String TEST_RESOURCE_NAME = "zap-quickscan.xml";

	private final ScanData scanData = new ScanData() {
		
		@Override
		public String getSessionId() {
			return UUID.randomUUID().toString();
		}
		
		@Override
		public List<ScanEntry> getScanEntries() {
			return null;
		}
		
		@Override
		public InputStream getInputStream(Predicate<String> matcher) throws IOException {
			return ClassLoader.getSystemResourceAsStream(TEST_RESOURCE_NAME);
		}
		
		@Override
		public InputStream getInputStream(ScanEntry scanEntry) throws IOException {
			return ClassLoader.getSystemResourceAsStream(TEST_RESOURCE_NAME);
		}
	};
	
	private final ScanBuilder scanBuilder = (ScanBuilder) Proxy.newProxyInstance(
			ZAPPluginTest.class.getClassLoader(),
			  new Class[] { ScanBuilder.class }, new InvocationHandler() {
				
				@Override
				public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
					System.err.println(method.getName()+": "+(args==null?null:Arrays.asList(args)));
					return null;
				}
			});
	
	private final VulnerabilityHandler vulnerabilityHandler = new VulnerabilityHandler() {
		
		@Override
		public StaticVulnerabilityBuilder startStaticVulnerability(String instanceId) {
			System.err.println("startStaticVulnerability: "+instanceId);
			return (StaticVulnerabilityBuilder) Proxy.newProxyInstance(
					ZAPPluginTest.class.getClassLoader(),
					  new Class[] { StaticVulnerabilityBuilder.class }, new InvocationHandler() {
						
						@Override
						public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {	
							System.err.println(method.getName()+": "+(args==null?null:Arrays.asList(args)));
							return null;
						}
					}); 
		}
	};
	
	@Test
	void testParseScan() throws Exception {
		new ZAPParserPlugin().parseScan(scanData, scanBuilder);
		// TODO Check actual output
	}
	
	@Test
	void testParseVulnerabilities() throws Exception {
		new ZAPParserPlugin().parseVulnerabilities(scanData, vulnerabilityHandler);
		// TODO Check actual output
	}

}
